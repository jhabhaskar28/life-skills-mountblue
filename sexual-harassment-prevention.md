# Prevention of Sexual Harassment

## What kinds of behaviour cause sexual harassment?

Sexual harassment is defined as any form of unwanted physical, visual or verbal conduct of sexual nature that violates a person's dignity while creating a hostile and humiliating work environment.

There are 3 categories of behaviour that cause sexual harassment:

1) **Physical**: It refers to behaviour that is of a physical nature. The following behaviour leads to physical sexual harassment:
	- Sexual assault
	- Inappropriate touching
	- Blocking movement
	- Sexual gestures 
	- Staring
	- Acts like kissing or hugging without consent

2) **Visual**: It can include the following:
	- Obscene pictures or drawings
	- Posters
	- Screensavers
	- Cartoons
	- Emails or texts of sexual nature

3) **Verbal**: It can include the following acts:
	- Foul language
	- Comments about the body or clothing
	- Sexual jokes or comments
	- Asking for sexual favours
	- Threats
	- Spreading rumours about a person's personal or sexual life
	- Repeatedly asking out a person

Further, there are 2 categories of sexual harassment:

- **Quid pro quo**: It includes using threats, punishments, or rewards as a tool for getting sexual favours. This is usually done by a person higher in the hierarchy than the victim.
- **Hostile work environment**: This occurs when the behaviour of fellow employees creates an environment that is hostile to the victim and obstructs the work performance of the person.

Prevention of sexual harassment is very much needed not only at the workplace but at all places, whether public or private so that the dignity of any individual is not violated and the individual can reach their full potential.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
My action will depend on the nature of such behaviour. If the incident is minor and happens for the first time, I will try to de-escalate the situation by talking to the person who has committed the act. In case of a non-minor incident or repeating incident, I will report the incident to the appropriate authorities following the sexual harassment prevention mechanism set up by the company. In the event of the incident being a major one, I will report the incident to the police. 

Sexual harassment is an evil that needs to be tackled on all fronts so that no person is inhibited from reaching his/her fullest potential. More awareness needs to be spread for the prevention of such behaviour so that such incidents can be minimized.
