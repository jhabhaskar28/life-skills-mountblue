# Focus Management

## 1. What is Deep Work

### What is Deep Work?

- Deep Work is the ability to focus without distraction on a cognitively demanding task
- It allows us to master complicated concepts in lesser time
- Deep Work can be used in any field to excel in that field
- It is also known as 'Hard Focus' 

## 4. Summary of Deep Work Book

### Paraphrase all the ideas in the above videos and this one **in detail**.

- Deep Work is defined as professional activities performed in a distraction-free environment that pushes our cognitive abilites to the limit
- These efforts increase our abilities and instill new values in us
- Famous personalities like Bill Gates and J.K Rowling have used deep work which lead to their success
- Intense periods of focus develops Myelin in the brain cells which allows us to think faster and clearer on the subject
- Deep work is very rare quality to have
- We should schedule our distractions in order to do deep work
- We should follow a deep work ritual which will train our mind to do deep work
- Early morning is the best time for deep work as distractions are minimum at that time
- Another way to cultivate deep work is to have an evening shutdown routine in order to take rest and get ready for deep work the following day

### How can you implement the principles in your day to day life?

I can implement these principles in the following ways:
- The use of mobile phone while working on something can be minimised
- I can try to focus harder on my work
- I can minimise distractions and noise while working
- I can follow a evening shutdown routine as explained in the book

## 5. Dangers of Social Media

### Your key takeaways from the video

My takeaways from this video are:
- Quitting social media will not make me unsocial
- Social media is only a source of entertainment
- Social media is designed to make us addictive to it
- Quitting social media will not mean that I am not competitive in the economy
- Lack of social media usage can help me focus more on my work and avoid distractions
- My life can be more positive without social media
