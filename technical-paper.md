# NoSQL


## Abstract

**NoSQL databases are rapidly gaining popularity and businesses are increasingly shifting from conventional databases to NoSQL databases mostly due to the speed, flexibility, and power they provide. This paper investigates whether the usage of a NoSQL database can tackle the performance and scaling issues of a project. Upon research, it is found that NoSQL databases offer significant advantages over conventional ones and that they can be quite helpful in improving the scalability and performance of a project. The top NoSQL database technologies available in the market are also found along with their relative advantages.**

## Introduction

**NoSQL** databases are non-tabular databases and store data differently than relational tables. These databases were developed to overcome the limitations of the conventional relational database models. Their main focus is on high operational speed and flexibility in storing data. NoSQL databases are used in real-time web applications and big data and their use is increasing over time. While relational databases can only manage structured data, NoSQL databases can handle complex, non-structured data, and that too of a very high volume. These databases also enable easy and frequent changes to the database. Because of all these advantages, their application is increasing over time and businesses are shifting from relational databases to NoSQL ones.

![NoSQL](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtn0kbxUtG7ytVMVsxPg605_b2rTLc3tKxRA&usqp=CAU)

## Types
NoSQL databases are one of the following four types:

-  **Tabular:**  Hbase, Big Table, Accumulo
-   **Graph Databases**: Amazon Neptune, Neo4j
-   **Key value store:**  Memcached, Redis, Coherence
-   **Document-based:**  MongoDB, CouchDB, Cloudant

## Pros and Cons of  NoSQL 

### Pros 
- **High scalability:** NoSQL systems use scale-out architecture which provides scalability when data volume or traffic grows.
- **Flexible storage:** Many different types of data, whether structured or semi-structured, can be stored and retrieved more easily.
- **Developer-friendly:** Developers find it easier to create various types of applications using NoSQL databases compared to using relational ones.
- **Minimum Downtime:** NoSQL databases take full advantage of the cloud to achieve this.
### Cons
- **Open-source:** There is no reliable standard for NoSQL yet. Two database systems are likely to be unequal.
-  **Management challenge:** Data management in NoSQL is much more complex than in a relational database.

## Why choose NoSQL?

A NoSQL database should be preferred for the following use cases :

-  Requirements for scale-out architecture.
-  Fast-paced Agile development.
-  Storage requirement of structured and semi-structured data.
-   Storage of vast volumes of data.

**Hence, a NoSQL database should be preferred over a relational database for the project to improve performance and rectify scaling issues.**

## Top NoSQL databases

1.  **[MongoDB](https://www.mongodb.com/)**: MongoDB is a general-purpose document-oriented database. It has a horizontal, scale-out architecture which means it can handle huge volumes of traffic and data thus making it **suitable for businesses having to store terabytes of data, or the database is going to be accessed by millions of users.** It is one of the most popular databases used by businesses recently.

2. **[MarkLogic](https://www.marklogic.com/)**: It is a multi-model NoSQL database **designed to handle complex data integration use cases such as large data sets with multiple models or in a fast-changing business environment.** It is compliant with ACID data principles making it consistent and reliable.

3. **[ScyllaDB](https://www.scylladb.com/)**: ScyllaDB is one of the fastest NoSQL databases out there. It should be **preferred when the business requirement is speed and cost-efficiency.** It is fully open-source and also provides great documentation support.

4. **[Apache Cassandra](https://cassandra.apache.org/)**: It is a free, open-source NoSQL database.  It is designed to be distributed across multiple servers, meaning that **the operations of a business will not collapse if there is an issue with a single server. It is popular with large enterprises that need a high level of consistency across their global business.**

5. **[Amazon DynamoDB](https://aws.amazon.com/dynamodb/)**: DynamoDB is a NoSQL database provided by Amazon as part of their Amazon Web Services suite. It sits completely in the cloud, meaning that the business does not need any hardware to use DynamoDB. It encrypts all data by default, and backups of data can be easily accessed. It is thus, **suitable when a business plans to launch a cloud-based product or if security and data loss prevention are important for the business.** 

6. **[Redis](https://redis.io/)**: This database is optimized for storing whole datasets in memory. Data is retrieved from the user device’s cache, thus, making it faster. It is a **great choice when the business requirement is to store and retrieve data in extremely short timeframes.**

## Conclusion 

**NoSQL databases offer a significant advantage in the form of high scalability, developer-friendliness, storage of complex data, and improving performance among others, thus, making it suitable for the project.**

**Each of the NoSQL database solutions mentioned in this document fit with different business requirements or use cases, and thus, should be chosen accordingly.**

## References 

- [https://www.mongodb.com/nosql-explained](https://www.mongodb.com/nosql-explained)
- [https://www.geeksforgeeks.org/introduction-to-nosql/](https://www.geeksforgeeks.org/introduction-to-nosql/)
- [https://datavid.com/blog/best-nosql-databases/](https://datavid.com/blog/best-nosql-databases/)
- [https://www.upwork.com/resources/nosql-vs-sql](https://www.upwork.com/resources/nosql-vs-sql)
- [https://www.couchbase.com/resources/why-nosql](https://www.couchbase.com/resources/why-nosql)
- [https://en.wikipedia.org/wiki/NoSQL](https://en.wikipedia.org/wiki/NoSQL)
- [https://www.danielleskosky.com/sql-nosql-whats-the-difference/](https://www.danielleskosky.com/sql-nosql-whats-the-difference/)
