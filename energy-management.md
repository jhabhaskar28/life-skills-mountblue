# Energy Management

## 1. Manage Energy not Time

### What are the activities you do that make you relax - Calm quadrant?

The activities that make me relax are:
- Listening to music
- Talking to friends
- Talking to family
- Outdoor walk
- Playing indoor games
- Listening, watching or reading philosophical content

### When do you find getting into the Stress quadrant?

I get into the stress quadrant when:
- There is expectation from me
- I have a deadline for a work
- I have pending work
- I am tested

### How do you understand if you are in the Excitement quadrant?

I understand I am in the excitement quadrant when:
- I achieve my goals
- Something good happens to me
- I meet my friends and family

## 4. Sleep is your superpower

### Paraphrase the Sleep is your Superpower video in detail.

The speaker starts with the ill effects of sleep deprivation on the reproductive health of humans. Sleep is also very important in the learning process. We need adequate sleep before learning something in order to grasp things better. An experiment was conducted in which there were two groups of people, one with adequate sleep and the other sleep deprived. It was found out that the sleep deprived group had around 40% reduced brain ability to make new memories. 
When we are in deep sleep, the brain waves transfer our memories from the temporary to the permanent section, making retention better. 

Sleep also has a direct impact on ageing. People age faster if they continue to loose sleep. Sleep has a direct impact on our cardiovascular system as well. Lack of sleep increases the chances of heart attack. Sleep deprivation leads to an increase in car crashes, road accidents and suicide rates. 

Sleep deprivation reduces the natural killer cells in our immune system making us vulnerable to diseases. Just a 4 hour decrease in sleep can reduce the natural killer cells in our immune system by around 70%. Thus, sleep deprivation has a direct relation with cancer risk in humans. The shorter the sleep, the shorter is the life. Sleep reduction also disrupts our gene activities. Thus, sleep has an impact on all aspects of our well being.

Finally, the speaker gives tips for better sleep which includes regularity of sleep and reducing the room temperature as it makes it easier to fall asleep.

### What are some ideas that you can implement to sleep better?

Some ideas to sleep better are:
- Having a fixed timetable for sleep
- Keeping the room temparature aroung 18 degree celcius
- Not using electronic devices before sleep
- Not indulging in physical activities before sleep

## 5. Brain Changing Benefits of Exercise

### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

The video discusses the following points as follows:
-  Exercise leads to better mood, better energy, better focus and better attention
- Exercise is the most transformative thing that could be done for the brain
- Exercise immediately increased brain activity by increasing neuro-transmitters like dopamine, adrenaline and ceratonin
- Exercise improves the reaction time in humans
- Exercise changes the brain's anatomy, phisiology and function
- Exercise leads to the production of brand new brain cells that increases the long term memory
- Regular exercise also increases our resistance against several diseases like Alzheimer's disease or dimentia

### What are some steps you can take to exercise more? 
Some steps that can be taken to exercise more are:
- Taking stairs instead of lift
- Avoiding transport and walking if feasible
- Indulging in any form of physical activity whenever possible
