# Learning Process

## 1. How to Learn Faster with the Feynman Technique

### What is the Feynman Technique? Paraphrase the video in your own words.

**Feynman technique** is a technique to learn anything faster and better. This technique is named after Richard Phillips Feynman, a noted American scientist who was known for his great explaining skills. The core of this technique is to have the ability to explain a concept in simple language in order to have a deeper understanding of the topic. 

This technique has 4 simple steps:
1. Choose a concept to learn about and write that on the top of a piece of paper.
2. Explain the concept to someone or pretend to do so in simple language. 
3. Identify the areas which could not be explained simply and improve your understanding of those areas.
4. Simplify the complicated parts by having a deeper understanding of those areas so that the whole concept could be explained simply.

This technique helps to increase efficiency in learning and minimize wastage of time. It also helps to have a deeper understanding of the topic or concept upon which this technique is applied.

### What are the different ways to implement this technique in your learning process?

The different ways in which this technique can be applied in the learning process can be summarised as follows:

- Explaining the concept or topic to a young kid and addressing their simple but tricky questions. 
- Pretending to explain to ourselves that concept and making sure there are no shaky areas.
- Using more and more examples to have a clear understanding of the subject of learning.
- Making diagrams wherever possible.

## 2. Learning How to Learn TED talk by Barbara Oakley

### Paraphrase the video in detail in your own words.

In the video, Barbara Oakley explains how she transformed her career from serving in the army to being a professor of engineering by changing herself from the inside. She explains the techniques to learn something effectively.

She divides the brain's operations into 2 parts: 
- **Focused Mode:** When the brain pays full attention to something avoiding other distractions. But here learning happens with the help of preconceived notions and ideas which results in a narrow way of learning.
- **Diffuse Mode:** Provides a new perspective to learning by allowing the brain to relax and think beyond. 

Both these modes need to work together in the learning process and the person has to back and forth between these two modes. She gives the examples of famous personalities like Dali and Edison who would relax to allow their brain to think about a problem loosely which helped them to come up with a solution to their problems. 

She also touches upon procrastination and how to avoid it. One way is to continue to work and not care about procrastination. The other way is to follow the Pomodoro technique and work with full focus for 25 minutes while avoiding all distractions and after that taking rest or doing a fun activity. 

She compares slow learners and fast learners and explains how slow learning is not bad at all using the hiker analogy. Slow learning has its own advantages like a more profound learning experience and having mastery over the topics. 

Finally, she explains the importance of tests and how they are fundamental to our understanding of a topic. Testing our understanding frequently makes sure that we are understanding what we are learning.

### What are some of the steps that you can take to improve your learning process?

Some steps that can be taken to improve our learning process include:
- Following the Pomodoro technique of working with focused attention for some time and then taking a rest.
- Allowing the brain to rest and think loosely to understand and simplify complex problems.
- Do not worry about slow learning as they are not as bad as they seem to be.
- Taking tests regularly and revising the topics again and again in order to have a deeper understanding of the topics.

## 3. Learn Anything in 20 hours

### Your key takeaways from the video? Paraphrase your understanding.

In this video, Josh Kaufman talks about how to learn anything. He busts some popular myths about learning something and shows his own research and even practically demonstrates it by learning to play the Ukulele and playing it to the crowd. Some of the key takeaways from the video include:

- The 10,000 hours rule to learn something new and become good at it only holds true when we wish to become an expert in a highly competitive field.
- We need to put only 20 hours of focussed attention on something to become really good at it.
- There is a method that can be followed to make the 20 hours of practice more efficient which includes simplifying the problem and removing distractions.
- We need to overcome the emotional barrier to learn something new. 

### What are some of the steps that you can while approaching a new topic?

While approaching a new topic, we can do the following steps:
- Breaking the problem into small parts and dealing with them one at a time.
- Start practicing as soon as possible with whatever we have learned.
- Removing distractions that divert our attention and focus on the work.
- Putting at least 20 hours of dedicated practice on the subject.
